﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFGetDataGridCell
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // データグリッドに列を3つ設定します。
            for (int i = 0; i < 3; ++i)
            {
                // 追加する列はテキストカラムとしてます。
                var column = new DataGridTextColumn();
                column.Header = $"列{i}";
                column.Binding = new Binding($"[{i}]");
                dataGrid1.Columns.Add(column);
            }

            // データグリッドに行を4つ設定します。
            var row1 = new string[] { "データ1-1", "データ1-2", "データ1-3" };
            var row2 = new string[] { "データ2-1", "データ2-2", "データ2-3" };
            var row3 = new string[] { "データ3-1", "データ3-2", "データ3-3" };
            var row4 = new string[] { "データ4-1", "データ4-2", "データ4-3" };
            var row5 = new string[] { "データ5-1", "データ5-2", "データ5-3" };
            var rows = new List<object>();
            rows.Add(row1);
            rows.Add(row2);
            rows.Add(row3);
            rows.Add(row4);
            rows.Add(row5);
            dataGrid1.ItemsSource = rows;
        }

        /**
         * @brief ウィンドウがロードされた後呼び出されます。
         * 
         * @param [in] sender ウィンドウ
         * @param [in] e イベント
         */
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // データグリッドの行数を取得します。
            var rowCount = dataGrid1.Items.Count;
            // データグリッドの行数分繰り返します。
            for (int i = 0; i < rowCount; ++i)
            {
                // データグリッドの行オブジェクトを取得します。
                var row = dataGrid1.ItemContainerGenerator.ContainerFromIndex(i) as DataGridRow;
                // 行オブジェクトが取得できない場合
                if (row == null)
                {
                    // 対象の行が表示されていない場合、行オブジェクトが取得できないため
                    // 対象の行が表示されるようスクロールします。
                    dataGrid1.UpdateLayout();
                    dataGrid1.ScrollIntoView(dataGrid1.Items[i]);
                    // 再度、行オブジェクトを取得します。
                    row = dataGrid1.ItemContainerGenerator.ContainerFromIndex(i) as DataGridRow;
                }

                // データグリッドの列数を取得します。
                var columnCount = dataGrid1.Columns.Count;
                // データグリッドの列数分繰り返します。
                for (int j = 0; j < columnCount; ++j)
                {
                    // データグリッドのセルオブジェクトを取得します。
                    var cell = dataGrid1.Columns[j].GetCellContent(row);
                    // データグリッドのセルオブジェクトが取得できない場合
                    if (cell == null)
                    {
                        // 対象のセルが表示されていない場合、セルオブジェクトが取得できないため
                        // 対象のセルが表示されるようスクロールします。
                        dataGrid1.UpdateLayout();
                        dataGrid1.ScrollIntoView(dataGrid1.Columns[j]);
                        // 再度、セルオブジェクトを取得します。
                        cell = dataGrid1.Columns[j].GetCellContent(row);
                    }

                    var textBlock = cell as TextBlock;
                    Console.WriteLine(textBlock.Text);
                }
            }
        }
    }
}
